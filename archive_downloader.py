import argparse

from aioresult import ResultCapture
from anyio import create_task_group, run, open_file, CapacityLimiter
from asynciolimiter import StrictLimiter
from csv import reader
from functools import partial
from httpx import AsyncClient, Limits, Timeout, TooManyRedirects
from pathlib import Path
from os import makedirs
from os.path import join, realpath, dirname, splitext, isfile, isdir
from tenacity import retry, stop_after_attempt, wait_random_exponential
from urllib.parse import quote, urlparse, parse_qs, unquote

from libs.utils import write_newlines_csv, delete_files, autodetect_encoding, read_json, modify_json, filter_csv


class ArchiveDownloader:

    def __init__(self):
        headers = {"User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:127.0) Gecko/20100101 Firefox/127.0'}
        self.session = AsyncClient(http2=True, headers=headers, default_encoding=autodetect_encoding, follow_redirects=True,
                                   limits=Limits(max_connections=10, max_keepalive_connections=10),
                                   timeout=Timeout(60.0, connect=60.0, read=60.0))
        self.throttler = StrictLimiter(10)
        self.semaphore = CapacityLimiter(10)
        self.parameters_dict = read_json()
        self.domain_link = None
        self.domain_folder = None
        self.counter = 0
        self.http_error_counter = 0
        self.error_counter = 0
        self.nr_of_pages = 0
        self.domain = None
        self.special_characters = [':', '<', '>', '"', '|', '?', '*', '%', '\\', '^', '#', '%', '&', '{', '}',
                                   ' ', '$', '!', "'", '@', '+', '=', '`', '~', '(', ')']

    async def wayback_machine_downloader(self, url, api='timemap', start_date="", end_date="",
                                         extensions_tuple=(), keywords_tuple=(), digest_tuple=(),
                                         exclude=False, only=False, contains=False, not_contains=False,
                                         exact=False, no_duplicates=False):

        user_dict = await self.create_user_dict(url, api, start_date, end_date, extensions_tuple, keywords_tuple,
                                                exclude, only, contains, not_contains, exact, no_duplicates)
        
        if api == 'timemap':
            from libs.timemap import Timemap
            api = Timemap()
        else:
            from libs.cdx import CDX
            api = CDX()
        
        self.domain = await self.get_main_domain(url)
        link = await self.edit_url(url)
        link = link.replace('/', '_')
        link = link.replace('.', '_')
        if len(link) > 250:
            link = link[:250]
        self.domain_folder = link
        
        if (self.parameters_dict['all_links_downloaded'] and
            isdir(f"websites/{self.domain_folder}") and
            user_dict.items() <= self.parameters_dict.items()):
            print('All links were downloaded')
            return
        
        self.parameters_dict['all_links_downloaded'] = False
        modify_json(self.parameters_dict)

        response = await self.session.get("https://web.archive.org")
        if response.status_code != 200:
            print("Web Archive is not available")
            return
        del response

        if (not user_dict.items() <= self.parameters_dict.items() or
            not isfile("pages.csv") or
            not self.parameters_dict['all_pages_downloaded']):

            await api.get_website_json(session=self.session, site=url, start_date=start_date, end_date=end_date, 
                                       extensions_tuple=extensions_tuple, keywords_tuple=keywords_tuple, digest_tuple=digest_tuple,
                                       exclude=exclude, only=only, contains=contains, not_contains=not_contains,
                                       exact=exact, no_duplicates=no_duplicates, domain=self.domain)
            
            for key, value in user_dict.items():
                self.parameters_dict[key] = value
            self.parameters_dict['all_pages_downloaded'] = True
            modify_json(self.parameters_dict)

        if not isfile("pages.csv"):
            print('No link pages were found')
            return

        if isfile('downloads.csv'):
            delete_files('downloads.csv')

        self.domain_link = url
        if not isdir(f"websites/{self.domain_folder}"):
            makedirs(f"websites/{self.domain_folder}")
            
        self.parameters_dict['site_folder'] = f"websites/{self.domain_folder}"
        modify_json(self.parameters_dict)
        
        print('Verifying if links were already downloaded')
        get_file_list = partial(self.get_files_list, no_duplicates)
        list_pages = list(reader(open("pages.csv", encoding="utf-8")))
        for start in range(0, len(list_pages), 50000):
            chunk = list_pages[start:start + 50000]
            async with create_task_group() as tg:
                futures = [ResultCapture.start_soon(tg, get_file_list, page) for page in chunk]
            results = [future.result() for future in futures if future.result()]
            if results:
                await write_newlines_csv('downloads.csv', results)
        del futures, list_pages, results, chunk

        if isfile('downloads.csv'):
            print(f'There are {len(tuple(reader(open("downloads.csv", encoding="utf-8"))))} links to download')
            print('Download is starting ...')

            self.nr_of_pages = len(tuple(reader(open("downloads.csv", encoding="utf-8"))))
            throttler = StrictLimiter(10)
            list_downloads = list(reader(open("downloads.csv", encoding="utf-8")))
            for start in range(0, len(list_downloads), 50000):
                chunk = list_downloads[start:start + 50000]
                async with create_task_group() as tg:
                    for file in chunk:
                        tg.start_soon(self.download_pages, throttler, file)
            del list_downloads, chunk
            await self.session.aclose()
            
            if self.http_error_counter == 0 and self.error_counter == 0:
                self.parameters_dict['all_links_downloaded'] = True
                modify_json(self.parameters_dict)
                print('All links were downloaded')
                delete_files('pages.csv')

            if self.http_error_counter != 0:
                print(f'{self.http_error_counter} links were skipped because they were badly archived.')

            if self.error_counter != 0:
                print(f'{self.error_counter} links failed to download because of errors.')
                print('These errors are caused by errors in the code or by connection closed by server.')
                print('Please check the links are indeed not working and if they are working try again later.')
                print('If the issue persists, please report it.')
        else:
            print('All links were already downloaded')

    async def create_user_dict(self, url, api, start_date, end_date, extensions_tuple, keywords_tuple,
                               exclude, only, contains, not_contains, exact, no_duplicates):
        parm_dict = {
            "site": url,
            "api": api,
            "start_date": start_date,
            "end_date": end_date,
            "extensions_tuple": extensions_tuple,
            "keywords_tuple": keywords_tuple,
            "only": only,
            "exclude": exclude,
            "contains": contains,
            "not_contains": not_contains,
            "exact": exact,
            "no_duplicates": no_duplicates,
        }
        return parm_dict

    async def get_main_domain(self, url):
        safe_chars = "!$&'()*+,-./:;=?@_~"
        url = quote(url, safe=safe_chars)
        parsed_url = urlparse(url)
        domain = parsed_url.netloc

        if not domain:
            domain = parsed_url.scheme

        if not domain:
            domain = parsed_url.path

        if ':' in domain:
            domain = domain.split(':')[0]

        parts = domain.split('.')
        if len(parts) > 2:
            domain = '.'.join(parts[-2:])
        
        return domain

    async def get_files_list(self, no_duplicates, page):
        file, page = await self.get_folder_tree_for_page(page, no_duplicates)
        page = (str(element) for element in page)
        path = join(*file)
        if not isfile(path):
            return [','.join(file), ','.join(page)]

    async def get_folder_tree_for_page(self, page, no_duplicates):
        timestamp, site = page
        site = await self.edit_url(site)
        parsed_url = urlparse(site)

        if parsed_url.path and self.domain not in parsed_url.path.split("/")[-1]:
            url_parts = parsed_url.path.split("/")
            file_name, file_extension = splitext(url_parts[-1])
            if len(file_name) > 250:
                file_name = file_name[:250]
            if file_extension:
                url_parts[-1] = file_name + file_extension
            else:
                url_parts[-1] = file_name + ".html"
            url = parsed_url.netloc + "/".join(url_parts)
        else:
            if len(site) > 250:
                site = site[:250]
            url = site + ".html"
        
        if url.endswith(','):
            url = url.rstrip(url[-1])
        url = url.replace(",", "/")

        url_parts = url.split("/")
        for i in range(len(url_parts) - 1):
            url_parts[i] = url_parts[i].replace(".", "_")
        url = "/".join(url_parts)

        site = "websites/" + self.domain_folder + '/' + url
        folder_tree = site.split("/")

        if not no_duplicates:
            folder_tree.insert(-1, str(timestamp))

        return folder_tree, page

    async def edit_url(self, url):
        url = unquote(url)

        if "http://" in url:
            url = url.replace("http://", "")

        if "https://" in url:
            url = url.replace("https://", "")

        url = url.replace(" ", "")
        parsed_url = urlparse(url)

        if parsed_url.query:
            url = url.split('?', 1)[0]
            query_params = parse_qs(parsed_url.query)
            filename, file_extension = splitext(url)
            for key, val in query_params.items():
                filename = filename + f'_{key}{val[0]}'
            url = filename + file_extension

        for special_character in self.special_characters:
            if special_character in url:
                url = url.replace(special_character, "")
        
        if url.endswith('/'):
            url = url.rstrip(url[-1])

        return url

    async def download_pages(self, throttler, file):
        await throttler.wait()
        folder_tree, page = file
        folder_tree = folder_tree.split(',')
        page = page.split(',', 1)
        url = "https://web.archive.org/web/" + page[0] + "if_" + "/" + page[1].replace(" ", "")
        path = join(*folder_tree)
        try:
            resp = await self.fetch_url(url)
            if resp:
                await self.create_folder_tree_from_dict(folder_tree)
                async with await open_file(path, "wb") as file:
                    await file.write(resp)
                await write_newlines_csv(f'websites/{self.domain_folder}/{self.domain_folder}.csv', [[path, url]])
                self.counter += 1
                print(f"{quote(url)} was downloaded ({self.counter}/{self.nr_of_pages})")
            else:
                await filter_csv("pages.csv", (page[0], page[1]))
                self.counter += 1
                print(f'{quote(url)} was skipped because it was badly archived ({self.counter}/{self.nr_of_pages})')
                self.http_error_counter += 1
        except Exception as e:
            self.counter += 1
            print(f'{quote(url)} failed to download with error: {e} ({self.counter}/{self.nr_of_pages})')
            self.error_counter += 1

    @retry(stop=stop_after_attempt(5), wait=wait_random_exponential(multiplier=5, max=60))
    async def fetch_url(self, url):
        await self.throttler.wait()
        async with self.semaphore:
            result = b""
            try:
                async with self.session.stream('GET', url) as response:
                    if response.status_code == 404:
                        return False
                    async for chunk in response.aiter_bytes():
                        result += chunk
            except TooManyRedirects:
                return False
            except Exception as e:
                raise e
            finally:
                if 'response' in locals():
                    await response.aclose()
            return result

    async def create_folder_tree(self, directory, current_path):
        for key, val in directory.items():
            Path(join(current_path, key)).mkdir(parents=True, exist_ok=True)
            if isinstance(val, dict):
                await self.create_folder_tree(val, join(current_path, key))

    async def create_folder_tree_from_dict(self, folder_tree):
        root = {}
        target_dict = root
        for component in folder_tree[0:-1]:
            target_dict = target_dict.setdefault(component, {})
        current_path = realpath(dirname(__file__))
        await self.create_folder_tree(root, current_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-w', '--website', dest='website', default='')
    parser.add_argument('-a', '--api', dest='api', default='timemap')
    parser.add_argument('-s', '--from', dest='start_year', default='')
    parser.add_argument('-e', '--to', dest='end_year', default='')
    parser.add_argument('-eu', '--exact-url', dest='exact', default=False)
    parser.add_argument('-o', '--only', dest='only', default=False)
    parser.add_argument('-x', '--exclude', dest='exclude', default=False)
    parser.add_argument('-c', '--contains', dest='contains', default=False)
    parser.add_argument('-nc', '--not-contains', dest='not_contains', default=False)
    parser.add_argument('-ex', '--extensions', dest='extensions', default=None)
    parser.add_argument('-k', '--keywords', dest='keywords', default=None)
    parser.add_argument('-d', '--digests', dest='digests', default=None)
    parser.add_argument('-st', '--single-timestamp', dest='no_duplicates', default=False)

    args = parser.parse_known_args()[0]
    wayback_machine_api = ArchiveDownloader()
    no_duplicates = False
    extensions = []
    keywords = []
    digests = []
    only = False
    exclude = False
    contains = False
    not_contains = False
    exact = False
    if args.no_duplicates == 'True':
        no_duplicates = True
    if args.exact == 'True':
        exact = True
    if args.extensions:
        for extension in args.extensions.split(','):
            extensions.append('.' + extension.strip())
    if args.keywords:
        for keyword in args.keywords.split(','):
            keywords.append(keyword.strip())
    if args.digests:
        for digest in args.digests.split(','):
            digests.append(digest.strip())
    if args.only == 'True':
        only = True
    if args.exclude == 'True':
        exclude = True
    if args.contains == 'True':
        contains = True
    if args.not_contains == 'True':
        not_contains = True

    async def main():
        await wayback_machine_api.wayback_machine_downloader(args.website, args.api, args.start_year, args.end_year,
                                                             tuple(extensions), tuple(keywords), tuple(digests),
                                                             exclude, only, contains, not_contains,
                                                             exact, no_duplicates)
    if args.website:
        run(main)
