import argparse

from anyio import run
from word_search import WordSearch


async def search_list_of_words(text_file, destination_folder):
    with open(text_file, 'r', encoding="utf-8") as f:
        words = f.read().split('\n')

        if '' in words:
            words.remove('')
        if ' ' in words:
            words.remove(' ')

    word_search = WordSearch()
    await word_search.word_search(words, destination_folder)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-wl', '--word-list', dest='word_list', default='')
    parser.add_argument('-tg', '--target-directory', dest='target_directory', default='')
    args = parser.parse_known_args()[0]

    async def main():
        await search_list_of_words(args.word_list, args.target_directory)
    
    if args.word_list and args.target_directory:
        run(main)
