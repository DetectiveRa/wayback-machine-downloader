import customtkinter
import psutil
import os
import tkinter
import webbrowser

from threading import Thread
from cchardet import detect
from subprocess import PIPE, Popen
from sys import exit
from libs.tkHyperlinkManager import HyperlinkManager
from functools import partial
from time import sleep
from urllib.parse import unquote

from libs.utils import read_json, delete_files

os.environ["PYTHONUNBUFFERED"] = "1"
customtkinter.set_appearance_mode("System")  # Modes: "System" (standard), "Dark", "Light"
customtkinter.set_default_color_theme("dark-blue")  # Themes: "blue" (standard), "green", "dark-blue"
download_pid = None
image_pid = None
word_pid = None


class App(customtkinter.CTk):
    def __init__(self):
        super().__init__()

        # get the screen dimension
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()

        # configure window
        window_width = round(screen_width*0.7)
        window_height = round(screen_height*0.7)

        # find the center point
        center_x = int(screen_width / 2 - window_width / 2)
        center_y = int(screen_height / 2 - window_height / 2)

        self.title("Wayback Machine Downloader")
        # set the position of the window to the center of the screen
        self.geometry(f"{window_width}x{window_height}+{center_x}+{center_y}")

        # configure grid layout (4x4)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure((2, 3), weight=0)
        self.grid_rowconfigure(8, weight=1)

        self.my_font = customtkinter.CTkFont(family="DejaVu")

        # create left sidebar frame with widgets
        self.sidebar_frame_left = customtkinter.CTkFrame(self, width=140, corner_radius=0)
        self.sidebar_frame_left.grid(row=0, column=0, rowspan=9, sticky="nsew")
        self.sidebar_frame_left.grid_rowconfigure(4, weight=1)

        self.word = customtkinter.CTkEntry(self.sidebar_frame_left, placeholder_text="Word")
        self.word.grid(row=0, column=0, padx=(20, 20), pady=(20, 0), sticky="nsew")

        self.search_word = customtkinter.CTkButton(master=self.sidebar_frame_left, text="Search word", border_width=2,
                                                   command=lambda: Thread(target=self.word_search, daemon=True).start())
        self.search_word.grid(row=1, column=0, padx=(20, 20), pady=(10, 0), sticky="nsew")

        self.search_list_words = customtkinter.CTkButton(master=self.sidebar_frame_left, text="Search list of words", border_width=2,
                                                         command=lambda: Thread(target=self.list_word_search, daemon=True).start())
        self.search_list_words.grid(row=2, column=0, padx=(20, 20), pady=(10, 0), sticky="nsew")

        self.search_image = customtkinter.CTkButton(master=self.sidebar_frame_left, text="Search image", border_width=2,
                                                    command=lambda: Thread(target=self.image_search, daemon=True).start())
        self.search_image.grid(row=3, column=0, padx=(20, 20), pady=(10, 0), sticky="nsew")

        self.logs = customtkinter.CTkCheckBox(master=self.sidebar_frame_left, text="Save output to log.txt",
                                                     onvalue=True, offvalue=False)
        self.logs.grid(row=5, column=0, padx=20, pady=(0, 10), sticky="nsew")

        self.appearance_mode_label = customtkinter.CTkLabel(self.sidebar_frame_left, text="Appearance Mode:", anchor="w")
        self.appearance_mode_label.grid(row=6, column=0, padx=20, pady=(10, 0))
        self.appearance_mode_option_menu = customtkinter.CTkOptionMenu(self.sidebar_frame_left,
                                                                       values=["Light", "Dark", "System"],
                                                                       command=self.change_appearance_mode_event)
        self.appearance_mode_option_menu.grid(row=7, column=0, padx=20, pady=(0, 10))
        self.scaling_label = customtkinter.CTkLabel(self.sidebar_frame_left, text="UI Scaling:", anchor="w")
        self.scaling_label.grid(row=8, column=0, padx=20)
        self.scaling_option_menu = customtkinter.CTkOptionMenu(self.sidebar_frame_left,
                                                               values=["80%", "90%", "100%", "110%", "120%"],
                                                               command=self.change_scaling_event)
        self.scaling_option_menu.grid(row=9, column=0, padx=20, pady=(0, 20))

        # create main entry and button
        self.website = customtkinter.CTkEntry(self, placeholder_text="Website")
        self.website.grid(row=0, column=1, columnspan=2, padx=(20, 20), pady=(20, 0), sticky="nsew")

        # create main entry and button
        self.extensions = customtkinter.CTkEntry(self, placeholder_text="Extensions (E.g.: jpg,jpeg,png)")
        self.extensions.grid(row=1, column=1, columnspan=2, padx=(20, 20), pady=(10, 0), sticky="nsew")

        self.keywords = customtkinter.CTkEntry(self, placeholder_text="Keywords (E.g.: anime,img,page)")
        self.keywords.grid(row=2, column=1, columnspan=2, padx=(20, 20), pady=(10, 0), sticky="nsew")

        # create textbox
        self.console = customtkinter.CTkTextbox(self, fg_color="#000000", text_color="green", font=self.my_font)
        self.console.grid(row=3, column=1, rowspan=8, padx=(20, 20), pady=(10, 10), sticky="nsew")

        # create right sidebar frame with widgets
        self.sidebar_frame_right = customtkinter.CTkFrame(self, width=140, corner_radius=0)
        self.sidebar_frame_right.grid(row=0, column=3, rowspan=10, sticky="nsew")
        self.sidebar_frame_right.grid_rowconfigure(8, weight=1)

        self.download_website = customtkinter.CTkButton(master=self.sidebar_frame_right, text="Download", border_width=2,
                                                        command=lambda: Thread(target=self.website_download, daemon=True).start())
        self.download_website.grid(row=0, column=3, padx=20, pady=(20, 0), sticky="nsew")

        self.extensions_option_menu = customtkinter.CTkOptionMenu(self.sidebar_frame_right,
                                                                  values=["No extensions filter", "Only", "Exclude"],
                                                                  command=self.change_extensions_status)
        self.extensions_option_menu.grid(row=1, column=3, padx=20, pady=(10, 0), sticky="nsew")

        self.keywords_option_menu = customtkinter.CTkOptionMenu(self.sidebar_frame_right,
                                                                values=["No keywords filter", "Contains", "Not contains"],
                                                                command=self.change_keywords_status)
        self.keywords_option_menu.grid(row=2, column=3, padx=20, pady=(10, 0), sticky="nsew")

        self.start_year = customtkinter.CTkEntry(self.sidebar_frame_right, placeholder_text="Start year")
        self.start_year.grid(row=3, column=3, padx=20, pady=(10, 0), sticky="nsew")

        self.end_year = customtkinter.CTkEntry(self.sidebar_frame_right, placeholder_text="End year")
        self.end_year.grid(row=4, column=3, padx=20, pady=(10, 0), sticky="nsew")

        self.exact_url = customtkinter.CTkCheckBox(master=self.sidebar_frame_right, text="Exact url",
                                                   onvalue=True, offvalue=False)
        self.exact_url.grid(row=5, column=3, padx=20, pady=(10, 0), sticky="nsew")

        self.single_timestamp = customtkinter.CTkCheckBox(master=self.sidebar_frame_right, text="Single timestamp",
                                                          onvalue=True, offvalue=False)
        self.single_timestamp.grid(row=6, column=3, padx=20, pady=(10, 0), sticky="nsew")

        self.api_option_menu = customtkinter.CTkOptionMenu(self.sidebar_frame_right, values=["timemap", "cdx"])
        self.api_option_menu.grid(row=7, column=3, padx=20, pady=(10, 0), sticky="nsew")

        self.stop_process = customtkinter.CTkButton(master=self.sidebar_frame_right, text="Stop Download/Search",
                                                     border_width=2, command=lambda: self.kill_process())
        self.stop_process.grid(row=10, column=3, padx=20, pady=(10, 0), sticky="nsew")

        self.clear_console = customtkinter.CTkButton(master=self.sidebar_frame_right, text="Clear console",
                                                     border_width=2, command=self.delete)
        self.clear_console.grid(row=11, column=3, padx=20, pady=(10, 20), sticky="nsew")

        # set default values
        self.appearance_mode_option_menu.set("System")
        self.scaling_option_menu.set("100%")
        self.extensions_option_menu.set("No extensions filter")
        self.keywords_option_menu.set("No keywords filter")
        self.extensions.configure(state=tkinter.DISABLED)
        self.keywords.configure(state=tkinter.DISABLED)
        self.console.configure(state=tkinter.DISABLED)
        self.search_word.configure(state=tkinter.DISABLED)
        self.search_list_words.configure(state=tkinter.DISABLED)
        self.word.configure(state=tkinter.DISABLED)
        self.search_image.configure(state=tkinter.DISABLED)
        self.hyperlink = HyperlinkManager(self.console)

    def change_appearance_mode_event(self, new_appearance_mode: str):
        customtkinter.set_appearance_mode(new_appearance_mode)

    def change_scaling_event(self, new_scaling: str):
        new_scaling_float = int(new_scaling.replace("%", "")) / 100
        customtkinter.set_widget_scaling(new_scaling_float)

    def change_extensions_status(self, filtering_type: str):
        if filtering_type == 'No extensions filter':
            self.extensions.configure(state=tkinter.DISABLED)
        else:
            self.extensions.configure(state=tkinter.NORMAL)

    def change_keywords_status(self, filtering_type: str):
        if filtering_type == 'No keywords filter':
            self.keywords.configure(state=tkinter.DISABLED)
        else:
            self.keywords.configure(state=tkinter.NORMAL)

    def delete(self):
        self.console.configure(state=tkinter.NORMAL)
        self.console.delete("0.0", customtkinter.END)
        self.console.configure(state=tkinter.DISABLED)

    def kill_process(self):
        self.console.insert(tkinter.END, '\nProcess was stopped')
        self.console.see(tkinter.END)
        global download_pid
        global word_pid
        global image_pid
        if download_pid:
            if psutil.pid_exists(download_pid):
                parent_process = psutil.Process(download_pid)
                for child_process in parent_process.children(recursive=True):
                    child_process.kill()
                parent_process.kill()
        if image_pid:
            if psutil.pid_exists(image_pid):
                parent_process = psutil.Process(image_pid)
                for child_process in parent_process.children(recursive=True):
                    child_process.kill()
                parent_process.kill()
        if word_pid:
            if psutil.pid_exists(word_pid):
                parent_process = psutil.Process(word_pid)
                for child_process in parent_process.children(recursive=True):
                    child_process.kill()
                parent_process.kill()

    def word_search(self):
        delete_files('log.txt')
        self.console.configure(state=tkinter.NORMAL)
        if self.word.get():
            parameters_dict = read_json()
            folder_path = parameters_dict['site_folder']
            command_line = f'python word_search.py -sw "{self.word.get()}" -tg {folder_path}'
            try:
                proc = Popen(command_line, shell=True, stdout=PIPE)
                global word_pid
                word_pid = proc.pid
                self.console.insert(tkinter.INSERT, '\n')
                for stdout_line in iter(proc.stdout.readline, b''):
                    self.update_display(stdout_line)
                    sleep(0.1)
            except Exception as e:
                self.console.insert(tkinter.INSERT, e)
        else:
            self.console.insert(tkinter.INSERT, '\nPlease write a word before pressing Search word')
        self.console.configure(state=tkinter.DISABLED)

    def list_word_search(self):
        delete_files('log.txt')
        self.console.configure(state=tkinter.NORMAL)
        filepath = tkinter.filedialog.askopenfilename(filetypes=[('Text files', '*.txt')])
        if filepath:
            parameters_dict = read_json()
            folder_path = parameters_dict['site_folder']
            command_line = f'python list_word_search.py -wl {filepath} -tg {folder_path}'
            try:
                proc = Popen(command_line, shell=True, stdout=PIPE)
                global word_pid
                word_pid = proc.pid
                self.console.insert(tkinter.INSERT, '\n')
                for stdout_line in iter(proc.stdout.readline, b''):
                    self.update_display(stdout_line)
                    sleep(0.1)
            except Exception as e:
                self.console.insert(tkinter.INSERT, e)
        self.console.configure(state=tkinter.DISABLED)

    def image_search(self):
        delete_files('log.txt')
        self.console.configure(state=tkinter.NORMAL)
        filepath = tkinter.filedialog.askopenfilename(filetypes=[('Image files', '*.jpeg *.jpg *.png')])
        if filepath:
            parameters_dict = read_json()
            folder_path = parameters_dict['site_folder']
            command_line = f'python image_search.py -si {filepath} -tg {folder_path}'
            try:
                proc = Popen(command_line, shell=True, stdout=PIPE)
                global image_pid
                image_pid = proc.pid
                self.console.insert(tkinter.INSERT, '\n')
                for stdout_line in iter(proc.stdout.readline, b''):
                    self.update_display(stdout_line)
                    sleep(0.1)
            except Exception as e:
                self.console.insert(tkinter.INSERT, e)
        self.console.configure(state=tkinter.DISABLED)

    def website_download(self):
        delete_files('log.txt')
        self.console.configure(state=tkinter.NORMAL)
        only = False
        exclude = False
        contains = False
        not_contains = False
        global download_pid
        if self.extensions_option_menu.get() == 'Only' and self.extensions.get():
            only = True
        if self.extensions_option_menu.get() == 'Exclude' and self.extensions.get():
            exclude = True
        if self.keywords_option_menu.get() == 'Contains' and self.keywords.get():
            contains = True
        if self.keywords_option_menu.get() == 'Not contains' and self.keywords.get():
            not_contains = True
        if self.website.get():
            command_line = f'python archive_downloader.py -w "{self.website.get()}" -a {self.api_option_menu.get()}' \
                           f' -o {only} -x {exclude} -c {contains} -nc {not_contains}' \
                           f' -eu {self.exact_url.get()} -st {self.single_timestamp.get()}'
            if self.start_year.get():
                command_line += f' -s {self.start_year.get()}'
            if self.end_year.get():
                command_line += f' -e {self.end_year.get()}'
            if self.extensions.get():
                command_line += f' -ex {self.extensions.get()}'
            if self.keywords.get():
                command_line += f' -k "{self.keywords.get()}"'
            try:
                proc = Popen(command_line, shell=True, stdout=PIPE)
                download_pid = proc.pid
                self.console.insert(tkinter.INSERT, '\nSearching cdx for pages with the website snapshots')
                self.console.insert(tkinter.INSERT, '\n')
                for stdout_line in iter(proc.stdout.readline, b''):
                    self.update_display(stdout_line)
                    sleep(0.1)
            except Exception as e:
                self.console.insert(tkinter.INSERT, e)
        else:
            self.console.insert(tkinter.INSERT, '\nPlease write a website before pressing Download')
        self.console.configure(state=tkinter.DISABLED)
        self.search_word.configure(state=tkinter.NORMAL)
        self.search_list_words.configure(state=tkinter.NORMAL)
        self.word.configure(state=tkinter.NORMAL)
        self.search_image.configure(state=tkinter.NORMAL)

    def update_display(self, next_line):
        encoding = detect(next_line).get("encoding") if detect(next_line).get("encoding") is not None else 'utf-8'
        next_line = next_line.decode(encoding, 'replace')
        self.mark_hyperlink(next_line)
        self.console.update_idletasks()
        self.console.see(tkinter.END)

    def mark_hyperlink(self, text):
        if self.logs.get():
            with open('log.txt', 'a') as f:
                f.write(unquote(text)) 
        word_list = text.split(' ')
        lines = int(self.console.index('end-1c').split('.')[0])
        if lines >= 10001:
            self.console.delete("1.0", f"{lines - 10000}.0")
        for word in word_list:
            if "http://" in unquote(word) or "https://" in unquote(word):
                self.console.insert(tkinter.END, word, self.hyperlink.add(partial(webbrowser.open, unquote(word))))
                self.console.insert(tkinter.END, ' ')
            else:
                self.console.insert(tkinter.END, word)
                self.console.insert(tkinter.END, ' ')


if __name__ == "__main__":
    app = App()
    app.mainloop()
    if download_pid:
        if psutil.pid_exists(download_pid):
            parent = psutil.Process(download_pid)
            for child in parent.children(recursive=True):
                child.kill()
            parent.kill()
    if image_pid:
        if psutil.pid_exists(image_pid):
            parent = psutil.Process(image_pid)
            for child in parent.children(recursive=True):
                child.kill()
            parent.kill()
    if word_pid:
        if psutil.pid_exists(word_pid):
            parent = psutil.Process(word_pid)
            for child in parent.children(recursive=True):
                child.kill()
            parent.kill()
    exit()
