@echo off

set venv_dir=venv

if not exist %venv_dir% (
    python -m venv %venv_dir%
    call %venv_dir%\Scripts\activate
    python -m pip install -r requirements.txt
    python initialize_clip_model.py
) else (
    call %venv_dir%\Scripts\activate
)
python Wayback_Machine_Downloader.py
:: Deactivate the virtual environment when done
%venv_dir%\Scripts\deactivate