#!/bin/bash

venv_dir="venv"
temp_dir="tmp"

if [ ! -d "$venv_dir" ]; then
    python3.12 -m venv "$venv_dir"
    source "$venv_dir/bin/activate"
    mkdir "$temp_dir"
    tmp_path=$(realpath "$temp_dir")
    pip install wheel
    export TMPDIR="$tmp_path"
    python3.12 -m pip install -r requirements.txt
    rm -r "$temp_dir"
    python3.12 initialize_clip_model.py
else
    source "$venv_dir/bin/activate"
fi

python3.12 Wayback_Machine_Downloader.py

# Deactivate the virtual environment when done
deactivate
