import click
import sys

from anyio import run

from archive_downloader import ArchiveDownloader
from word_search import WordSearch
from list_word_search import search_list_of_words
from list_digest_search import search_list_of_digests
from image_search import ImageSearch
from libs.utils import read_json, delete_files

@click.command()
@click.option('-w', '--website', 'website', default='', help='Website URL')
@click.option('-a', '--api', 'api', default='timemap', help='API to use (timemap/cdx), default is timemap')
@click.option('-f', '--from', 'start_year', default='', help='Start year')
@click.option('-t', '--to', 'end_year', default='', help='End year')
@click.option('-eu', '--exact-url', 'exact', default=False, help='Exact URL match (default is False)')
@click.option('-o', '--only', 'only', default=False, help='Includes only specified extensions (default is False)')
@click.option('-x', '--exclude', 'exclude', default=False, help='Excludes specified extensions (default is False)')
@click.option('-ex', '--extensions', 'extensions', default=None, help='Extensions (E.g.: jpg,jpeg,png)')
@click.option('-c', '--contains', 'contains', default=False, help='Includes only specified keywords (default is False)')
@click.option('-nc', '--not_contains', 'not_contains', default=False, help='Excludes specified keywords (default is False)')
@click.option('-k', '--keywords', 'keywords', default=None, help='Keywords (E.g.: anime,img,page)')
@click.option('-d', '--digests', 'digests', default=None,
              help='Digests, separate them with commas like in the keywords/extensions examples')
@click.option('-dl', '--digest-list', 'digest_list', type=click.Path(exists=True, dir_okay=False, file_okay=True),
              default=None, help='Path to input text file with list of digests')
@click.option('-st', '--single-timestamp', 'no_duplicates', default=False, help='Single timestamp (default is False)')
@click.option('-sw', '--search-word', 'search_word', default='', help='Search for a specific word')
@click.option('-wl', '--word-list', 'word_list', type=click.Path(exists=True, dir_okay=False, file_okay=True),
              default=None, help='Path to input text file with list of words')
@click.option('-si', '--source-image', 'source_image',type=click.Path(exists=True, dir_okay=False, file_okay=True),
              default=None, help='Path to source image')
@click.option('-tg', '--target-directory', 'target_directory', type=click.Path(exists=True, dir_okay=True, file_okay=False),
              default=None, help='Path to folder where the files that will be search are located')
@click.option('-qs', '--quick-search', 'quick_search', default=False, 
              help='Search for a word/list of words/image in the last downloaded domain (default is False)')
@click.option('-l', '--log', 'log', default=False, help='Saves output to log.txt (default is False)')

def cli(website, api, start_year, end_year, exact, only, exclude, extensions, no_duplicates,
        contains, not_contains, keywords, digests, digest_list, log,
        search_word, word_list, source_image, target_directory, quick_search):

    if (only or exclude) and not extensions:
        click.echo("Error: The '--extensions' option is required when '--only' or '--exclude' is used.")
        return
    
    if extensions and not (only or exclude):
        click.echo("Error: The '--extensions' option must be only used when '--only' or '--exclude' is used.")
        return
    
    if (contains or not_contains) and not keywords:
        click.echo("Error: The '--keywords' option is required when '--contains' or '--not_contains' is used.")
        return
    
    if keywords and not (contains or not_contains):
        click.echo("Error: The '--keywords' option must be only used when '--contains' or '--not_contains' is used.")
        return
    
    if quick_search:
        parameters_dict = read_json()
        folder_path = parameters_dict['site_folder']
        if folder_path:
            target_directory = folder_path
        else:
            click.echo("Error: Download a website first before running quick search.")
            return
    if log:
        delete_files('log.txt')
        backup = sys.stdout
        f = open('log.txt', 'a')
        sys.stdout = Tee(sys.stdout, f)
    
    if digest_list and website:
        run(run_async_function, search_list_of_digests, digest_list, website)
    elif website:
        extensions_list = []
        keywords_list = []
        digests_list = []
        if extensions:
            extensions = extensions.split(',')
            extensions_list = ['.' + extension.strip() for extension in extensions]
        if keywords:
            keywords = keywords.split(',')
            keywords_list = [keyword.strip() for keyword in keywords]
        if digests:
            digests = digests.split(',')
            digests_list = [digest.strip() for digest in digests]

        wayback_machine_api = ArchiveDownloader()
        run(run_async_function, wayback_machine_api.wayback_machine_downloader, website, api, start_year,end_year,
            tuple(extensions_list), tuple(keywords_list), tuple(digests), exclude, only, contains, not_contains,
            exact, no_duplicates)
    elif search_word and target_directory:
        find_word = WordSearch()
        run(run_async_function, find_word.word_search, search_word, target_directory)
    elif word_list and target_directory:
        run(run_async_function, search_list_of_words, word_list, target_directory)
    elif source_image and target_directory:
        find_image = ImageSearch()
        run(run_async_function, find_image.image_search, source_image, target_directory)
    else:
        click.echo("No arguments were provided or wrong combination of arguments was provided")
    
    if log:
        sys.stdout = backup

async def run_async_function(fun, *args, **kwargs):
    await fun(*args, **kwargs)

class Tee(object):
    def __init__(self, *files):
        self.files = files
    def write(self, obj):
        for f in self.files:
            f.write(obj)
    

if __name__ == '__main__':
    cli()