import argparse

from anyio import run
from re import findall

from archive_downloader import ArchiveDownloader


async def search_list_of_digests(text_file, website):                    
    with open(text_file, 'r', encoding="utf-8") as f:
        content = f.read()
        digests = findall(r'([A-Z0-9]{32})', content)

    digest_search = ArchiveDownloader() 
    await digest_search.wayback_machine_downloader(website, digest_tuple=tuple(digests))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-dl', '--digest-list', dest='digest_list', default='')
    parser.add_argument('-w', '--website', dest='website', default='')
    args = parser.parse_known_args()[0]

    async def main():
        await search_list_of_digests(args.digest_list, args.website)
    
    if args.digest_list and args.website:
        run(main)