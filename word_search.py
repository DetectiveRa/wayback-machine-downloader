import argparse

from ahocorasick_rs import AhoCorasick
from anyio import create_task_group, run, open_file, sleep, to_thread, Semaphore
from csv import reader
from datetime import timedelta
from functools import partial
from os.path import isfile
from selectolax.parser import HTMLParser
from unicodedata import normalize

from libs.utils import files_to_csv, delete_files, verify_path_to_url_exists, autodetect_encoding


class WordSearch:

    def __init__(self):
        self.extensions = ['.htm', '.html', '.shtml', '.php', '.dat', '.cgi']
        self.path_to_url_list = None
        self.word_found = False
        self.counter = 0
        self.aho = None
        self.semaphore = Semaphore(100)

    async def word_search(self, search_word, folder_path):
        print('Searching all web files in folder')
        csv_path = await files_to_csv(folder_path, self.extensions)
        files_list = list(reader(open(csv_path, encoding="utf-8")))
        nr_of_files = len(files_list)
        search_list = set(search_word if isinstance(search_word, list) else [search_word])
        search_list = [normalize('NFKC', word).casefold() for word in search_list]
        self.aho = AhoCorasick(search_list)
        estimated_total_time = int(nr_of_files * (0.0044 + (max(0, len(search_list) - 1)) * 0.4 / nr_of_files))
        path_list = folder_path.split("/")
        if isfile(f'{folder_path}/{path_list[-1]}.csv'):
            with open(f'{folder_path}/{path_list[-1]}.csv', "r", encoding="utf-8") as file:
                self.path_to_url_list = list(reader(file))
        print(f'There are {nr_of_files} files')
        print(f'Estimated time: {str(timedelta(seconds=estimated_total_time))}')

        search_file = partial(self.search_file, search_list)
        print('Searching word ...')
        for start in range(0, len(files_list), 10000):
            chunk = files_list[start:start + 10000]
            async with create_task_group() as tg:
                for file in chunk:
                    tg.start_soon(search_file, file[0])
            self.counter += 10000
            if self.counter < nr_of_files:
                print(f'{self.counter} files processed')

        delete_files(csv_path)
        print('\nAll files were searched')
        if not self.word_found:
            print('\nWord not found')

    async def search_file(self, search_list, folder_tree):
        async with self.semaphore:
            if not isfile(folder_tree):
                return

            async with await open_file(folder_tree, 'rb') as file:
                search_file = await file.read()
                encoding = autodetect_encoding(search_file)
                search_file = search_file.decode(encoding, 'replace')

            html_parser = HTMLParser(search_file)
            body = html_parser.body
            if not body:
                return

            hrefs = {node.attrs.get('href', '') for node in html_parser.css('a[href]')}
            hrefs = {href for href in hrefs if href is not None}
            srcs = {node.attrs.get('src', '') for node in html_parser.css('img[src]')}
            srcs = {src for src in srcs if src is not None}
            visible_text = await to_thread.run_sync(html_parser.body.text, True, " ", True)
            visible_text = ' '.join(dict.fromkeys(visible_text.split()))
            text = f"{visible_text} {' '.join(hrefs)}" if hrefs else visible_text
            text = f"{visible_text} {' '.join(srcs)}" if srcs else text
            if not self.aho:
                return

            matches = set(self.aho.find_matches_as_strings(normalize('NFKC', text).casefold(), overlapping=True))
            if not matches:
                return

            link = await verify_path_to_url_exists(self.path_to_url_list, folder_tree)
            print(f'\n{",".join(matches)} found in {folder_tree}')
            if link:
                print(link)
            self.word_found = True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-sw', '--search-word', dest='search_word', default='')
    parser.add_argument('-tg', '--target-directory', dest='target_directory', default='')
    args = parser.parse_known_args()[0]
    find_word = WordSearch()

    async def main():
        await find_word.word_search(args.search_word, args.target_directory)

    if args.search_word and args.target_directory:
        run(main)
