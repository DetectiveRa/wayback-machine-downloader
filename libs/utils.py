from aiocsv import AsyncWriter
from anyio import Lock, open_file
from cchardet import detect
from csv import reader
from json import dump, load
from os.path import isfile, splitext, join
from os import remove, walk, replace
from urllib.parse import quote

lock = Lock()
async def write_newlines_csv(file_name, data):
    async with lock:
        async with await open_file(file_name, "a", newline="", encoding='utf-8') as file:
            csv_file = AsyncWriter(file)
            await csv_file.writerows(data)


async def filter_csv(file_name, data):
    filename, file_extension = splitext(file_name)
    temp_file_name = filename + "_tmp" + file_extension
    async with lock:
        with open(file_name, "r", encoding="utf-8") as file:
            original_list = list(reader(file))
        filtered_list = [row for row in original_list if row[0] != data[0] and row[1] != data[1]]
    
        async with await open_file(temp_file_name, "w", newline="", encoding='utf-8') as file:
            filtered_csv = AsyncWriter(file)
            await filtered_csv.writerows(filtered_list)
        
        replace(temp_file_name, file_name)


def delete_files(file_name):
    if isfile(file_name):
        remove(file_name)


def autodetect_encoding(content):
    return detect(content).get("encoding") if detect(content).get("encoding") is not None else 'utf-8'


def create_default_json():
    default_dict = {
            "site": None,
            "api": None,
            "start_date": None,
            "end_date": None,
            "extensions_tuple": None,
            "keywords_tuple": None,
            "only": False,
            "exclude": False,
            "contains": False,
            "not_contains": False,
            "exact": False,
            "no_duplicates": False,
            "all_pages_downloaded": False,
            "all_links_downloaded": False,
            "site_folder": None
        }
    with open("parameters.json", 'w') as file:
        dump(default_dict, file, indent=2)


def read_json():
    if not isfile("parameters.json"):
        create_default_json()
    with open("parameters.json", 'r') as file:
        parameters_dict = load(file)
        if parameters_dict["extensions_tuple"] is not None:
            parameters_dict["extensions_tuple"] = tuple(parameters_dict["extensions_tuple"])
        if parameters_dict["keywords_tuple"] is not None:
            parameters_dict["keywords_tuple"] = tuple(parameters_dict["keywords_tuple"])
    return parameters_dict


def modify_json(modified_parameters_dict):
    if not isfile("parameters.json"):
        create_default_json()
    with open("parameters.json", 'w') as file:
        dump(modified_parameters_dict, file, indent=2)


async def files_to_csv(folder_path, extensions):
    delete_files(f'{folder_path}/files.csv')
    files = []
    for root, subFolders, filenames in walk(folder_path):
        for filename in filenames:
            if splitext(filename)[1].lower() in extensions:
                files.append([join(root, filename)])
    await write_newlines_csv(f'{folder_path}/files.csv', files)
    return f'{folder_path}/files.csv'

async def verify_path_to_url_exists(path_to_url_list, folder_tree):
    url = []
    if path_to_url_list:
        for row in path_to_url_list:
            if row[0] in folder_tree:
                url.append(row[1])
        if url:
            return f'Wayback machine link: {quote(url[0])}'