from ahocorasick_rs import AhoCorasick
from aioresult import ResultCapture
from anyio import create_task_group, CapacityLimiter
from asynciolimiter import StrictLimiter
from csv import reader
from functools import partial
from msgspec import json
from os.path import isfile, splitext
from tenacity import retry, stop_after_attempt, wait_random_exponential
from urllib.parse import quote, urlparse, unquote

from libs.utils import write_newlines_csv, delete_files


class Timemap:

    def __init__(self):
        self.aho = None
        self.page_number = 0
        self.nr_of_pages = 0
        self.counter = 0
        self.domain = None
        self.throttler = StrictLimiter(10)
        self.semaphore = CapacityLimiter(10)

    async def get_website_json(self, session, site, start_date="", end_date="",
                               extensions_tuple=(), keywords_tuple=(), digest_tuple=(),
                               exclude=False, only=False, contains=False, not_contains=False,
                               exact=False, no_duplicates=False, domain=None):

        self.domain = domain
        timemap_query = await self.create_timemap_url(site, no_duplicates, exact, start_date, end_date, digest_tuple)
        timemap_query_pages = timemap_query + '&showNumPages=true'
        pages_req = await session.get(timemap_query_pages)
        if pages_req.status_code == 403:
            print("Error code 403: Forbidden: This type of CDX query requires authorization.")
            return
        if pages_req.status_code == 503:
            print("Error code 503: Server is not responding.")
            return
        self.nr_of_pages = int(pages_req.json()[1][0])
        print(f'There are {self.nr_of_pages} pages with max 53640 links per page')
        timemap_query += "&fl=timestamp,original,digest"
        delete_files('pages.csv')
        print('Getting links from wayback machine using timemap API')
        throttler = StrictLimiter(10)
        download_pages = partial(self.download_pages, session, throttler, extensions_tuple, keywords_tuple, digest_tuple,
                                 exclude, only, contains, not_contains, timemap_query)
        for start in range(0, self.nr_of_pages, 50000):
            end = start + 50000 if start + 50000 < self.nr_of_pages else self.nr_of_pages
            chunk = range(start, end)
            async with create_task_group() as tg:
                for page_number in chunk:
                    tg.start_soon(download_pages, page_number)

        if isfile('pages.csv'):
            print(f'There are {len(list(reader(open("pages.csv", encoding="utf-8"))))} links')

    async def create_timemap_url(self, site: str, no_duplicates: bool, exact: bool, start_date: str, end_date: str, digest: tuple):
        timemap_query = "https://web.archive.org/web/timemap/json?"
        match_type = "domain"

        if "http://" in site:
            site = site.replace("http://", "")
        if "https://" in site:
            site = site.replace("https://", "")
        if ":80" in site:
            site = site.replace(":80", "")
        if ":443" in site:
            site = site.replace(":443", "")
        if "*." in site:
            site = site.replace("*.", "")
        if '/*' in site:
            site = site.replace("/*", "")
            match_type = "prefix"
        timemap_query += f"url={quote(site.encode('utf-8'))}"

        if no_duplicates:
            timemap_query += "&collapse=urlkey"

        if exact:
            match_type = "exact"

        timemap_query += f"&matchType={match_type}"

        if start_date != "":
            timemap_query += f"&from={start_date}"

        if end_date != "":
            timemap_query += f"&to={end_date}"

        if digest or no_duplicates:
            timemap_query += "&filter=statuscode:200"
        else:
            timemap_query += "&filter=statuscode:[2-3][0-9][0-9]"

        if digest:
            timemap_query += "&filter=!mimetype:text/*|unk"

        timemap_query += "&pageSize=9"

        return timemap_query

    async def get_snapshots_list(self, extensions_tuple, keywords_tuple, digest_tuple, exclude, only, contains, not_contains,
                                 website, url):
        website_json = json.decode(website.content)
        if not website_json:
            print(f'{quote(url)} contains 0 links ({self.counter}/{self.nr_of_pages})')
            return
        website_json = website_json[1:]
        filter_list = partial(self.filter_list, extensions_tuple, keywords_tuple, digest_tuple, exclude, only , contains, not_contains)
        async with create_task_group() as tg:
            futures = [ResultCapture.start_soon(tg, filter_list, page) for page in website_json]
        website_json = [future.result() for future in futures if future.result()]
        if website_json:
            print(f'{quote(url)} contains {len(website_json)} links after filtering ({self.counter}/{self.nr_of_pages})')
        else:
            print(f'{quote(url)} contains 0 links ({self.counter}/{self.nr_of_pages})')
        return website_json

    async def filter_list(self, extensions_tuple, keywords_tuple, digest_tuple, exclude, only, contains, not_contains, page):
        site = page[1].lower()
        safe_chars = "!$&'()*+,-./:;=?@_~"
        site = quote(site, safe=safe_chars)
        parsed_url = urlparse(site)

        if parsed_url.path and self.domain not in parsed_url.path.split("/")[-1]:
            url_parts = parsed_url.path.split("/")
            file_name, file_extension = splitext(url_parts[-1])
            if not file_extension:
                file_extension = ".html"
        else:
            file_extension = ".html"
        site = unquote(site)

        if 'robots.txt' not in site:
            if not only and not exclude and not contains and not not_contains and not digest_tuple:
                return page[:2]

            if only or exclude:
                page = await self.filter_extensions(extensions_tuple, exclude, only, file_extension, page)

            if contains or not_contains and page is not None:
                page = await self.filter_keywords(keywords_tuple, contains, not_contains, site, page)

            if digest_tuple and page is not None:
                page = await self.filter_digests(digest_tuple, page)

            if page is not None:
                return page[:2]

    async def filter_extensions(self, extensions_tuple, exclude, only, file_extension, page):
        self.aho = None
        extensions_tuple = tuple({extension.casefold() for extension in extensions_tuple})
        self.aho = AhoCorasick(extensions_tuple)
        if not self.aho:
           return

        matches = set(self.aho.find_matches_as_strings(file_extension.casefold(), overlapping=True))

        if matches:
            if only:
                return page
            if exclude:
                return None

        return None if only else page

    async def filter_keywords(self, keywords_tuple, contains, not_contains, site, page):
        self.aho = None
        keywords_tuple = tuple({keyword.casefold() for keyword in keywords_tuple})
        self.aho = AhoCorasick(keywords_tuple)
        if not self.aho:
           return

        matches = set(self.aho.find_matches_as_strings(site.casefold(), overlapping=True))

        if matches:
            if contains:
                return page
            if not_contains:
                return None

        return None if contains else page

    async def filter_digests(self, digest_tuple, page):
        if page[2] in digest_tuple:
            return page

    async def download_pages(self, session, throttler, extensions_tuple, keywords_tuple, digest_tuple,
                             exclude, only, contains, not_contains, timemap_query, page_number):
        await throttler.wait()
        timemap_query += f'&page={page_number}'
        try:
            resp = await self.fetch_url(session, timemap_query)
            if resp:
                self.counter += 1
                website_json = await self.get_snapshots_list(extensions_tuple, keywords_tuple, digest_tuple,
                                                             exclude, only, contains, not_contains,
                                                             resp, timemap_query)
                if website_json:
                    await write_newlines_csv('pages.csv', website_json)
        except Exception as e:
            print(f'{quote(timemap_query)} failed to download with error: {e}')
            return

    @retry(stop=stop_after_attempt(5), wait=wait_random_exponential(multiplier=5, max=60))
    async def fetch_url(self, session, url):
        await self.throttler.wait()
        async with self.semaphore:
            response = await session.get(url)
            response.raise_for_status()
            return response
