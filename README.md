# Wayback Machine Downloader

Download an entire website from the Wayback Machine.

## Installation

Download the latest version for your os:
- [windows](https://gitlab.com/DetectiveRa/wayback-machine-downloader/-/blob/main/release_archives/wayback_machine_downloader_windows.zip?ref_type=heads)
- [linux](https://gitlab.com/DetectiveRa/wayback-machine-downloader/-/blob/main/release_archives/wayback_machine_downloader_linux.zip?ref_type=heads)
- [macos](https://gitlab.com/DetectiveRa/wayback-machine-downloader/-/blob/main/release_archives/wayback_machine_downloader_macos.zip?ref_type=heads)

For windows:

1. Install these requirements:
- Python: Install python 3.12 from microsoft store
- vc_redist (necessary for ai image search): https://aka.ms/vs/16/release/vc_redist.x64.exe

2. Restart your pc

3. Double click on RunDownloaderWindows.bat


For linux:

1. Make sure you have installed python venv and python tk packages for your distribution

    - For Debian-based Linux: 
      - ```sudo apt-get update```
      - ```sudo apt-get install python3-tk python3-venv python-is-python3```
    - For Arch-based Linux: 
      - ```sudo pacman -S tk python-virtualenv```
    - For Fedora-based Linux: 
      - ```sudo dnf install python3-tkinter python3-virtualenv python-unversioned-command```

2. Give execution permission to RunDownloaderLinux.sh

3. Open a terminal in the folder where you downloaded the wayback machine downloader and copy this command ```./RunDownloaderLinux.sh``` in terminal


For MacOS:

1. Open terminal applicationa and run these commands

    - ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"```
    - ```brew install python```
    - ```brew install python-tk```

2. Add python to MacOS path <br />
    ```nano ~/.bash_profile```
    
    Add this line: ```export PATH="$PATH:/Library/Frameworks/Python.framework/Versions/3.12/bin"``` <br />
    Ctrl + X to exit and save when prompted using Enter

3. Restart terminal and change path to where wayback machine downloader folder is downloaded <br />
    Example for Downloads folder: ```cd ~/Downloads/wayback_machine_downloader_macos```

    To open the program: ```./RunDownloaderMacOS.sh```

    Note: During first run the program will install all dependencies

## How to fix [WinError 206] The filename or extension is too long, windows issue:

Open the Registry Editor:

    Press Win + R to open the Run dialog.
    Type regedit and press Enter to open the Registry Editor. If prompted by User Account Control, click Yes.

Navigate to the Long Path Key:

    In the Registry Editor, navigate to the following key:

    HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem

Modify the LongPathsEnabled Value:

    Look for a value named LongPathsEnabled. If it doesn't exist, you will need to create it.
    To create the value, right-click on the right pane, select New > DWORD (32-bit) Value, and name it LongPathsEnabled.
    Double-click on LongPathsEnabled and set the value to 1.
    Click OK and close the Registry Editor.

Restart Your Computer

## How to fix issues with corrupted characters or UTF errors in CMD, windows issue:

Go to Control Panel -> Region -> Administrative -> Change system Locale

Check BETA: Use Unicode UTF-8 for worldwide language support and press OK

Restart Your Computer

If the issue persists change Current system locale to the country that hosted the site you are downloading <br />
E.g.: Japanese (Japan) if you are downloading japanese sites