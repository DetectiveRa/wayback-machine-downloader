import argparse
import cv2
import torch

from aioresult import ResultCapture
from anyio import create_task_group, run, to_thread
from csv import reader
from datetime import timedelta
from PIL import Image
from os.path import isfile
from torch.cuda import is_available
from transformers import CLIPImageProcessor, CLIPModel
from sentence_transformers import util

from libs.utils import files_to_csv, delete_files, verify_path_to_url_exists


class ImageSearch:

    def __init__(self):
        self.device = "cuda" if is_available() else "cpu"
        torch.set_default_device(self.device)
        self.time_per_file = 0.005 if self.device == "cuda" else 0.02
        self.model = CLIPModel.from_pretrained("facebook/metaclip-b32-fullcc2.5b", cache_dir='cache').to(self.device).eval()
        for param in self.model.parameters():
            param.requires_grad = False
        self.model = torch.compile(self.model, dynamic=True, backend="inductor", mode="reduce-overhead", fullgraph=True).to(self.device)
        self.preprocess = CLIPImageProcessor.from_pretrained("facebook/metaclip-b32-fullcc2.5b")
        self.image_found = False
        self.counter = 0
        self.extensions = ['.png', '.jpg', '.jpeg', '.bmp', '.dib', '.webp', '.sr', '.ras ', '.tif', '.jfif', '.xxx']
        self.path_to_url_list = None 

    async def image_search(self, source_image, folder_path):
        print('Searching all images in folder')
        csv_path = await files_to_csv(folder_path, self.extensions)
        nr_of_files = len(list(reader(open(csv_path, encoding="utf-8"))))
        path_list = folder_path.split("/")
        if isfile(f'{folder_path}/{path_list[-1]}.csv'):
            with open(f'{folder_path}/{path_list[-1]}.csv', "r", encoding="utf-8") as file:
                self.path_to_url_list = list(reader(file))
        print(f'There are {nr_of_files} images')
        print(f'Estimated time: {str(timedelta(seconds=int(nr_of_files * self.time_per_file)))}')
        
        input_image = await to_thread.run_sync(self.preprocess_image, source_image)
        if input_image is not None:
            input_color_histogram = await to_thread.run_sync(self.extract_color_histogram, source_image)
            input_image = self.preprocess(input_image, return_tensors="pt")["pixel_values"].to(self.device)
            with torch.no_grad():
                input_embedding = self.model.get_image_features(input_image).to(self.device)
                input_embedding /= input_embedding.norm(dim=-1, keepdim=True)
        else:
            print("Image could not be processed")
            return

        print("Searching sample image ...")
        images_list =  list(reader(open(csv_path, encoding="utf-8")))
        for start in range(0, len(images_list), 1000):
            chunk = images_list[start:start + 1000]
            async with create_task_group() as tg:
                futures = [ResultCapture.start_soon(tg, self.compare_images, file[0]) for file in chunk]
            paths = [future.result()[0] for future in futures if future.result()]
            images = [future.result()[1] for future in futures if future.result()]
            color_histograms = [future.result()[2] for future in futures if future.result()]
            if not images:
                return
            
            images = [self.preprocess(image, return_tensors="pt")["pixel_values"].to(self.device) for image in images]
            with torch.no_grad():
                images_embeddings = self.model.get_image_features(torch.cat(images)).to(self.device)
                images_embeddings /= images_embeddings.norm(dim=-1, keepdim=True)
            async with create_task_group() as tg:
                for folder_tree, image_embedding, color_histogram in zip(paths, images_embeddings, color_histograms):
                    tg.start_soon(self.calculate_similarity_score, folder_tree, input_embedding, image_embedding, input_color_histogram, color_histogram)
            
            self.counter += 1000
            if self.counter < nr_of_files:
                print(f'{self.counter} files processed')


        delete_files(csv_path)
        print('\nAll files were searched')
        if not self.image_found:
            print('\nNo similar images were found')

    async def compare_images(self, folder_tree):
        image = await to_thread.run_sync(self.preprocess_image, folder_tree)
        image_color_histogram = await to_thread.run_sync(self.extract_color_histogram, folder_tree)
        if image is not None and image_color_histogram is not None:
            return folder_tree, image, image_color_histogram
        
    async def calculate_similarity_score(self, folder_tree, input_embedding, image_embedding, input_color_histogram, image_color_histogram):
        similarity_score = util.dot_score(input_embedding, image_embedding)
        similarity_score = round(float(similarity_score[0][0]), 3)
        similarity_score = round((similarity_score + 1) / 2, 3)
        color_similarity = round(cv2.compareHist(input_color_histogram, image_color_histogram, cv2.HISTCMP_CORREL), 3)
        combined_similarity = round((similarity_score * 0.7) + (color_similarity * 0.3), 3)
        if combined_similarity >= 0.87 and similarity_score >= 0.853 and color_similarity >= 0.788:
            link = await verify_path_to_url_exists(self.path_to_url_list, folder_tree)
            print(f'\nSimilar image found in {folder_tree}')
            if link:
                print(link)
            self.image_found = True
    
    def extract_color_histogram(self, path):
        try:
            if not isfile(path):
                return

            image = cv2.imread(path)
            if image is None:
                return

            image = cv2.resize(image, (512, 512))
            lab_image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
            l, a, b = cv2.split(lab_image)
            clahe = cv2.createCLAHE(clipLimit=5)
            l_eq = clahe.apply(l)
            lab_eq_image = cv2.merge((l_eq, a, b))
            image = cv2.cvtColor(lab_eq_image, cv2.COLOR_LAB2RGB)
            hist = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
            cv2.normalize(hist, hist)
            return hist.flatten()
        except Exception:
            return

    def preprocess_image(self, path):
        try:
            if not isfile(path):
                return
            
            image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
            if image is None:
                return
            image = cv2.resize(image, (512, 512))
            image = Image.fromarray(image)
            return image
        except Exception:
            return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-si', '--source-image', dest='source_image', default='')
    parser.add_argument('-tg', '--target-directory', dest='target_directory', default='')
    args = parser.parse_known_args()[0]
    find_image = ImageSearch()

    async def main():
        await find_image.image_search(args.source_image, args.target_directory)

    if args.source_image and args.target_directory:
        run(main)
